obus (1.2.5-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Fri, 07 Jun 2024 21:20:52 +0200

obus (1.2.4-3) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Use ocaml_dune DH buildsystem

  [ Debian Janitor ]
  * Set upstream metadata fields

 -- Stéphane Glondu <glondu@debian.org>  Thu, 17 Aug 2023 07:27:42 +0200

obus (1.2.4-2) unstable; urgency=medium

  * Team upload.
  * Bump standards-version to 4.6.2.
  * Fix compilation with recent dune (Closes: #1040233).

 -- Julien Puydt <jpuydt@debian.org>  Tue, 04 Jul 2023 08:57:32 +0200

obus (1.2.4-1) unstable; urgency=medium

  * Add patch to support newer ppxlib.
  * Bump standards-version to 4.6.1.
  * Fix d/watch.
  * New upstream.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 14 Oct 2022 09:02:49 +0200

obus (1.2.3-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Wed, 26 Aug 2020 11:50:53 +0200

obus (1.2.2-1) unstable; urgency=medium

  * New upstream release (Closes: #836118)
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0

 -- Stéphane Glondu <glondu@debian.org>  Wed, 29 Jul 2020 13:34:02 +0200

obus (1.2.1-1) unstable; urgency=medium

  [ Stéphane Glondu ]
  * New upstream release (Closes: #933992)
  * Update Homepage and debian/watch
  * Update Vcs-*
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.4.1
  * Add Rules-Requires-Root: no

  [ Nicolas Dandrimont ]
  * d/control: remove myself from Uploaders

 -- Stéphane Glondu <glondu@debian.org>  Mon, 20 Jan 2020 09:09:35 +0100

obus (1.1.5-6) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Add ocamlbuild to Build-Depends
  * Update Vcs-*

  [ Dimitri John Ledkov ]
  * Remove duplicate constructor, to fix FTBFS with ocaml 4.04.

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Jul 2017 07:08:02 +0200

obus (1.1.5-5) unstable; urgency=medium

  * Fix compilation with react 1.0.0

 -- Stéphane Glondu <glondu@debian.org>  Thu, 05 Nov 2015 10:31:56 +0100

obus (1.1.5-4) unstable; urgency=medium

  * Add camlp4 to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Fri, 16 Oct 2015 10:29:44 +0200

obus (1.1.5-3) unstable; urgency=low

  * Upload to unstable (Closes: #713365)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Dec 2013 09:40:44 +0100

obus (1.1.5-2) experimental; urgency=low

  * Build-Depend on ocaml-find >= 1.4 to handle new native architectures

 -- Stéphane Glondu <glondu@debian.org>  Sat, 27 Jul 2013 10:03:52 +0200

obus (1.1.5-1) experimental; urgency=low

  * New upstream release
    - needs OCaml >= 4 and type-conv >= 108
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Sat, 27 Jul 2013 09:18:19 +0200

obus (1.1.4-2) unstable; urgency=low

  * Do not build natively on !natdynlink (fixes FTBFS)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 10 May 2013 19:49:57 +0200

obus (1.1.4-1) unstable; urgency=low

  * New upstream release
  * Use format version 1.0 in debian/copyright
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compat level to 8

 -- Stéphane Glondu <glondu@debian.org>  Thu, 09 May 2013 23:23:13 +0200

obus (1.1.3-1) unstable; urgency=low

  * New upstream release
    - patch applied upstream: Fix-for-type-conv-2.3.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 30 Jul 2011 17:19:05 +0200

obus (1.1.2-2) unstable; urgency=low

  * Disable all tests needing a working DBus daemon
  * Make the tests use a temporary directory as $HOME (Closes: #630322)
  * Force compilation as bytecode on architectures without native dynlink
    (Closes: #630320)

 -- Nicolas Dandrimont <nicolas.dandrimont@crans.org>  Tue, 28 Jun 2011 21:39:32 +0200

obus (1.1.2-1) unstable; urgency=low

  * New upstream release
    - Drop patches fixed upstream
    - Add patch for compatibility with type-conv >= 2.3.0
  * Bump required OCaml version to 3.12.0, lwt to 2.2.0
  * Bump Standards-Version to 3.9.2 (no changes)
  * Upload to unstable

 -- Nicolas Dandrimont <nicolas.dandrimont@crans.org>  Sat, 11 Jun 2011 23:33:06 +0200

obus (1.0~rc1-2) experimental; urgency=low

  * Rebuild with OCaml 3.11.2
  * Switch source package format to 3.0 (quilt)
    - convert existing changes to
      0001-Fix-build-on-bytecode-architectures.patch
  * Add 0002-Fix-build-with-lwt-2.1.0.patch
  * Bump Standards-Version to 3.8.4 (no changes)
  * Fix debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Thu, 29 Apr 2010 08:28:02 +0200

obus (1.0~rc1-1) experimental; urgency=low

  * Initial release (Closes: #550754)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 16 Oct 2009 10:51:54 +0200
